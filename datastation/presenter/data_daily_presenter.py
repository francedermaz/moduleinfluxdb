from sqlite3 import Timestamp
from time import time
from controller.exchange_controller import alltickets
from datetime import datetime
import calendar

alljson = []

for row in alltickets:
    now = datetime.now()
    now = str(now)
    now = now[8:10] + '/' + now[5:7] + '/' + now[2:4] + ' 00:00:00.000'
    now = datetime.strptime(now, '%d/%m/%y %H:%M:%S.%f')
    timestamp = calendar.timegm(now.utctimetuple())
    timestamp = str(timestamp)
    timestamp = timestamp + '000000000'
    timestamp = int(timestamp)
    json_body = [
    {
        "measurement": "Data",
        "time":timestamp,
        "tags":{
            "pair":row["pair"],
            "exchange":row["exchange"],
        },
        "fields":{
            "close":row["close"],
            "open":row["open"],
            "max":row["max"],
            "min":row["min"],
        }
    }]
    alljson.append(json_body)

