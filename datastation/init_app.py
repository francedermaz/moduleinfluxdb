from infraestructure.influx_adapter import client
from presenter.data_daily_presenter import alljson
for json in alljson:
    client.write_points(json)