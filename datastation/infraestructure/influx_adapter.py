from influxdb import InfluxDBClient
import os
from dotenv import load_dotenv

dotenv_path = './config/.env'
load_dotenv(dotenv_path)

hostenv = os.environ.get('INFLUX_HOST')
portenv = os.environ.get('INFLUX_PORT')
db = os.environ.get('INFLUX_DB')

client = InfluxDBClient(host=hostenv,port=portenv)
client.switch_database(db)