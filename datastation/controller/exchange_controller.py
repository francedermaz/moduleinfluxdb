import yaml
from infraestructure.CCXT_adapter import allexchanges

with open('./config/data_config.yaml') as f:
    data = yaml.load(f, Loader=yaml.FullLoader)

alltickets = []

print('Requesting...')

for exchname in data["exchanges"]:
    for exchangeccxt in allexchanges:
        if exchname == exchangeccxt["exchange_id"]:
            for elem in data["exchanges"][exchname]:
                ticket = exchangeccxt["exchange"].fetchTicker(elem)
                ticket = { "timestamp":ticket["timestamp"],
                        "pair":ticket["symbol"],
                        "exchange":exchangeccxt["exchange_id"],
                        "close":ticket["close"],
                        "open":ticket["open"],
                        "max":ticket["high"],
                        "min":ticket["low"]}
                alltickets.append(ticket)