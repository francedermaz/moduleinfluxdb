import ccxt
import os
import yaml
from dotenv import load_dotenv

dotenv_path = './config/.env'
load_dotenv(dotenv_path)

with open('./config/exchanges_config.yaml') as f:
    data = yaml.load(f, Loader=yaml.FullLoader)

allexchanges = []

for elem in data["exchanges"]:
    exchange_id = elem
    exchange_class = getattr(ccxt, exchange_id)
    exchange = exchange_class({
        'apiKey': os.environ.get('API_'+ exchange_id.upper() + '_KEY'),
        'secret': os.environ.get('API_'+ exchange_id.upper() + '_SECRET_KEY'),
    })
    allexchanges.append({'exchange_id':exchange_id,'exchange':exchange})