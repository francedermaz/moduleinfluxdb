import schedule
import time
import pathlib
import os
from dotenv import load_dotenv

dotenv_path = './config/.env'
load_dotenv(dotenv_path)

timeenv = os.environ.get('TIME')
timevar = int(timeenv[:-1])

pathis = pathlib.Path().resolve()
str(pathis)
pathis=pathis.as_posix()+'/init_app.py'

def job():
    os.system(pathis)
    print('Data saved on database')

if timeenv[len(timeenv)-1]=='M':
    schedule.every(timevar).minutes.do(job)
if timeenv[len(timeenv)-1]=='H':
    schedule.every(timevar).hours.do(job)

job() # Lo corre ahora
while True:
    schedule.run_pending() # Lo corre cada x minutos / horas
    time.sleep(1)